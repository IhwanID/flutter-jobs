import 'package:flutter/material.dart';
import 'package:flutterjobs/detail.dart';
import 'jobs.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Jobs'),
        backgroundColor: Colors.cyan,
      ),
      body: ListView.builder(
          itemCount: getJobs().length,
          itemBuilder: (context, index) {
            return Container(
              decoration: new BoxDecoration(
                  color: Colors.cyan,
                  borderRadius: new BorderRadius.all(Radius.circular(20))),
              padding: EdgeInsets.only(top: 8, bottom: 8),
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              height: 150,
              child: ListTile(
                title: Center(
                  child: Column(
                    children: <Widget>[
                      Image.asset('images/${getJobs()[index].photo}.png'),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        getJobs()[index].name,
                        style: TextStyle(color: Colors.white, fontSize: 21),
                      ),
                    ],
                  ),
                ),
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailPage(
                              job: getJobs()[index],
                            ))),
              ),
            );
          }),
    );
  }
}
