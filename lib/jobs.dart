class JobPlatform {
  final int id;
  final String name;
  final String description;
  final String photo;
  final String link;

  JobPlatform({this.id, this.name, this.description, this.photo, this.link});
}

List<JobPlatform> getJobs() {
  return [
    JobPlatform(
        id: 1,
        name: 'Dicoding Jobs',
        description:
            "Menghubungkan Anak Bangsa Terbaik pada Pilihan Karir yang Relevan. Bangun Karir dengan Bekerja di Perusahaan Ternama Jaringan adalah salah satu hal penting yang tidak dimiliki oleh semua orang dalam mendapatkan kesempatan kerja yang layak.\n\nDengan menjadi anggota komunitas Developer Dicoding, hal tersebut bisa kamu dapatkan dengan mudah. Lowongan di Perusahaan Nasional & Multinasional Lelah kerja di perusahaan tanpa jenjang karir yang jelas? Kerja keras tapi fasilitas tak sepadan? Temukan kesempatan kerja yang lebih baik di Dicoding Job Platform.\n\nMempersempit Persaingan Dicoding Job Board menawarkan kesempatan kerja yang secara eksklusif disebar di kalangan Developer Dicoding Indonesia saja. Portofolio di Dicoding Fresh graduate dan kesempatan kerja terbatas? Jika aktif di Dicoding dan punya portofolio yang bagus di sini, kamu punya kesempatan sama dengan mereka yang berpengalaman.",
        photo: 'dicoding',
        link: 'https://www.dicoding.com/jobs'),
    JobPlatform(
        id: 1,
        name: 'Tech in Asia',
        description:
            "Job search for people passionate about startups. Easily & quickly search thousands of jobs from Asia’s tech scene.\n\nHere’s why you’ll love it Apply with confidence We only allow serious jobs on our platform and encourage companies to disclose benefits & culture upfront. No missed opportunities We’ll curate and notify you about new jobs you may like. You can now put that fear of missing out to rest. \n\nProactive employers Coming soon: with your consent, employers can headhunt and reach out to you proactively. Update your status to be discovered. Tech & startup jobs only Search and apply to active tech & startup jobs, posted on a regular basis. Quick & easy Set up your profile and apply for jobs in just a few minutes. It’s that simple.",
        photo: 'techinasia',
        link: 'https://www.techinasia.com/jobs'),
    JobPlatform(
        id: 2,
        name: 'Projects.co.id',
        description:
            "Projects.co.id adalah “Project and Digital Product Marketplace” atau tempat transaksi (menawarkan project dan mencari project) secara online antara owner (pemberi kerja/ pengguna jasa) dan worker/freelancer khususnya Freelancer Indonesia (pekerja/tenaga ahli).\n\nProjects.co.id juga menyediakan tempat untuk terjadinya transaksi penjualan product digital antara seller (penjual product) dan buyer (pembeli product) Dengan demikian Projects.co.id sebagai online marketplace pertama di dunia yang menggabungkan dua konsep besar, dua konsep besar yang dimaksud adalah: Transaksi project (menawarkan project dan mencari project), antara owner (pemberi kerja) dan worker/freelancer (pekerja/tenaga ahli). \n\n“Semua pekerjaan yang hasil akhirnya bisa dikemas dalam bentuk file” bisa dikerjakan di Projects.co.id Transaksi Jual Beli Produk Digital antara seller (penjual produk) dan buyer (pembeli produk) “Semua produk digital yang bisa dikemas dalam bentuk file” bisa diperjualbelikan di space penjualan Projects.co.id.\n\nProjects.co.id bertindak sebagai “Mediator, Fasilitator dan Penjamin”  agar terjadi hubungan kerja yang win-win antara  Owner dan Worke Owner  menerima hasil kerja sesuai dengan yang diinginkan dan worker  menerima bayaran sesuai dengan yang dijanjikan Buyer dan Selle Buyer menerima produk sebagaimana yang diinginkan dan seller menerima bayaran sesuai harga penjuala Untuk meningkatkan kenyamanan bagi pengguna, telah tersedia fitur Service. Melalui service Sekarang worker/seller bisa mempromosikan keahliannya pada pembuatan project untuk suatu pekerjaan tertentu Buyer semakin mudah untuk mendapatkan suatu produk digital yang benar-benar sesuai dengan kebutuhannya Owner juga semakin mudah untuk membuat project yang dibutuhkannya.",
        photo: 'projects',
        link: 'https://projects.co.id/'),
    JobPlatform(
        id: 1,
        name: 'Sribulancer',
        description:
            'Sribulancer membantu bisnis dan konsumer untuk mencari freelancer bertalenta di seluru dunia, mulai dari freelance programer, website desainer, desainer grafis, voice over, pembuat video, penulis artikel dan banyak lagi. Jasa yang diberikan fokus kepada kecepatan, akurasi dan kemudahan.',
        photo: 'sribulancer',
        link: ''),
    JobPlatform(
        id: 1,
        name: 'Fastwork',
        description:
            'Fastwork adalah salah satu platform freelance online yang dapat membantu pemilik bisnis untuk menemukan dan mempekerjakan freelancer secara sederhana, cepat, aman, dan hemat biaya untuk mempercepat pertumbuhan bisnis mereka. Fastwork mengklaim lebih dari 300.000 bisnis di seluruh Asia Tenggara menggunakan 22.000 layanannya, mulai dari desain grafis, pemasaran online dan influencer, entri data hingga pengembangan web dan aplikasi. Di sisi lain, Fastwork juga mendukung tenaga kerja untuk dapat bekerja dari mana pun dan kapan pun.\n\nSistem Fastwork membantu freelancer dalam mengumpulkan pembayaran, mempromosikan layanan, mengelola pesanan, bertukar file, dan berkomunikasi dengan klien mereka. Didirikan pada 2015 oleh sekelompok insinyur dan entrepreneur dari Silicon Valley dan New York, Fastwork adalah platform freelance profesional terbesar di Asia Tenggara saat ini, berdasarkan jumlah pengguna dan proyek yang diselesaikan.',
        photo: 'fastwork',
        link: 'https://fastwork.id'),
    JobPlatform(
        id: 1,
        name: 'Qerja',
        description:
            'Qerja.com adalah komunitas online / portal web pertama di Indonesia untuk berbagi informasi gaji dan review perusahaan / lingkungan kerja, khususnya untuk perusahaan yang berlokasi di Asia Tenggara',
        photo: 'qerja',
        link: 'https://www.qerja.com/'),
  ];
}
